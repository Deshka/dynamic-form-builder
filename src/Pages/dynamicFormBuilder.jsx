import React, { useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';
import { Input, Select, Text, Checkbox } from '../Components';

import './dynamicFormBuilder.css';
export const DynamicFormBuilder = (props) => {
  const [elements, setElements] = useState([]);
  const [state, setState] = useState({
    firstname: '',
    lastname: '',
    country: '',
    subscribe: false,
    email: ''
  });

  useEffect(() => {
    setElements(props.formJSON);
  }, []);

  const onSubmit = (event) => {
    console.log(`{
      First Name: ${state.firstname},
      Last Name: ${state.lastname},
      Country: ${state.country},
      Subscribed to newsletter: ${state.subscribe ? 'Yes' : 'No'},
      ${state.subscribe ? `Email: ${state.email}` : ''}
     }`);
    event.preventDefault();
    setState({
      firstname: '',
      lastname: '',
      country: '',
      subscribe: false,
      email: ''
    });
  };

  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    setState({
      ...state,
      [name]: value
    });
  };

  const textFields = () => {
    return elements.map((el) => {
      switch (el.type) {
        case 'text':
          return (
            <Text
              key={el.id}
              name={el.id}
              label={el.label}
              type={el.type}
              placeholder={el.placeholder}
              value={el.id === 'firstname' ? state.firstname : state.lastname}
              required={el.required}
              onChange={handleInputChange}
            />
          );
        case 'input':
          return (
            <Input
              key={el.id}
              name={el.id}
              label={el.label}
              type={el.type}
              placeholder={el.placeholder}
              value={state.email}
              required={el.required}
              onChange={handleInputChange}
            />
          );
        case 'select':
          return (
            <Select
              key={el.id}
              name={el.id}
              label={el.label}
              value={state.country}
              required={el.required}
              options={el.options}
              onChange={handleInputChange}
            />
          );
        case 'checkbox':
          return (
            <Checkbox
              key={el.id}
              name={el.id}
              label={el.label}
              required={el.required}
              checked={state[el.id]}
              onChange={handleInputChange}
            />
          );
        default:
          return null;
      }
    });
  };
  return (
    <div>
      <Form onSubmit={onSubmit} className="form-wrapper">
        {textFields()}
        <input type="submit" value="Submit" />
      </Form>
    </div>
  );
};
