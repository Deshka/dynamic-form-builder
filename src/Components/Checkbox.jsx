import React from 'react';
import { Form } from 'react-bootstrap';
export const Checkbox = (props) => {
  const { name, label, required, checked, ...rest } = props;
  return (
    <div className="field-wrapper" >
      <Form.Check
        name={name}
        label={label}
        required={required}
        checked={checked}
        {...rest}
      />
      
    </div>
  );
};
