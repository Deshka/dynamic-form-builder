import React from 'react';
import { Form } from 'react-bootstrap';
export const Select = (props) => {
  const { name, label, value, required, options, onChange, errors, ...rest } = props;
  return (
    <div className="field-wrapper">
      <Form.Select
        className="field"
        name={name}
        label={label}
        required={required}
        onChange={onChange}
        value={value}
        {...rest}
      >
        <option value="" disabled hidden>
          {label}
        </option>
        {options.map((option) => (
          <option key={option.id} value={option.label}>{option.label}</option>
        ))}
      </Form.Select>
      {errors?.name && <p className="error">{errors.name}</p>}
    </div>
  );
};
