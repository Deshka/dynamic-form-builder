import React from 'react';
import { Form } from 'react-bootstrap';
export const Text = (props) => {
  const {name, label, type, placeholder, value, required,errors, ...rest  } = props;
  return (
    <div className="field-wrapper" >
      <Form.Group className="field-wrapper">
        <Form.Label>{label}</Form.Label>
        <br />
        <Form.Control
          className="field"
          name={name}
          type={type}
          placeholder={placeholder}
          value={value}
          required={required}
          {...rest}
        />
        <br />
      </Form.Group>
      {errors?.name && <p className="error">{errors.name}</p>}
    </div>
  );
};
