
import './App.css';
import formJSON from './formData.json';
import { DynamicFormBuilder } from './Pages/dynamicFormBuilder';


function App() {
  return (
    <div className="container">
      <DynamicFormBuilder formJSON={formJSON[0].fields} />
    </div>
  );
}

export {App};
