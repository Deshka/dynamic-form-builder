
import React, { Component } from 'react';
import { Form } from 'react-bootstrap';
import './dynamicFormBuilder.css';

class DynamicFormBuilder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      country: '',
      subscribe: false,
      emailVisibility: false,
      email: '',
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.hideComponent = this.hideComponent.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }
  hideComponent() {
    this.setState({
      emailVisibility: !this.state.emailVisibility,
    });
  }
  handleSubmit(event) {
    console.log(`{
     First Name: ${this.state.firstname},
     Last Name: ${this.state.lastname},
     Country ${this.state.country},
     Subscribed to newsletter: ${this.state.subscribe ? 'Yes' : 'No'},
     ${this.state.subscribe ? `Email: ${this.state.email}` : ''}
    }`);
    this.setState({
      firstname: '',
      lastname: '',
      country: '',
      subscribe: false,
      emailVisibility: false,
      email: '',
    });
    event.preventDefault();
  }
  textFields() {
    return this.props.formJSON.map((el) => {
      switch (el.type) {
        case 'text':
          return (
            <Form.Group key={el.id} className="field-wrapper">
              <Form.Label>{el.label}</Form.Label>
              <br />
              <Form.Control
                className="field"
                name={el.id}
                type={el.type}
                placeholder={el.placeholder}
                value={
                  el.id === 'firstname'
                    ? this.state.firstname
                    : this.state.lastname
                }
                required={el.required}
                onChange={this.handleInputChange}
              />
              <br />
            </Form.Group>
          );
        case 'select':
          return (
            <div className="field-wrapper" key={el.id}>
              <Form.Select
                className="field"
                name={el.id}
                label={el.label}
                required={el.required}
                value={this.state.country}
                onChange={this.handleInputChange}
              >
                <option value={el.value}>{el.label}</option>
                {el.options.map((option) => (
                  <option key={option.id}>{option.label}</option>
                ))}
              </Form.Select>
            </div>
          );
        case 'checkbox':
          return (
            <div className="field-wrapper" key={el.id}>
              <Form.Check
                name={el.id}
                label={el.label}
                required={el.required}
                checked={this.state.subscribe}
                onChange={this.handleInputChange}
                onClick={this.hideComponent}
              />
            </div>
          );
        case 'input':
          return this.state.emailVisibility ? (
            <Form.Group key={el.id} className="field-wrapper">
              <Form.Label>{el.label}</Form.Label>
              <br />
              <Form.Control
                className="field"
                name={el.id}
                type={el.type}
                placeholder={el.placeholder}
                value={this.state.email}
                required={el.required}
                onChange={this.handleInputChange}
              />
              <br />
            </Form.Group>
          ) : null;

        default:
          return null;
      }
    });
  }
  render() {
    return (
      <div>
        <Form onSubmit={this.handleSubmit} className="form-wrapper">
          {this.textFields()}
          <input type="submit" value="Submit" />
        </Form>
      </div>
    );
  }
}
export default DynamicFormBuilder;
