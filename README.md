# ***Dynamic Form Builder***
<br>

## Table of contents
   - [1. Description](#1-description)
   - [2. Project information](#2-project-information)
   - [3. Setup](#3-setup)
   - [4. Project structure](#4-project-structure)



### **1. Description**
<br>

Dynamic form builder - deployed to [Gitlab page](https://deshka.gitlab.io/dynamic-form-builder/)

<br>

### **2. Project information**

<br>

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**, **React**
- Libraries: **react-bootstrap**, **Material-table**

<br>

### **3. Setup**
<br>

You will be working in the `src` folder. This will be designated as the **root** folder, where `package.json` should be placed.

You need to install all the packages in the root folder: `npm i`.

The project can be run in two ways:

- `npm run eslint` - will run the **eslint** file 
- `npm start` - root client starts the frontend part in folder client **root**, where `package.json` should be placed


### 4. Project structure

**Front-end**

- `src/App.js` - the entry point of the frontend project
<br>